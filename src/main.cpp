
#include "ImplCommands.h"
#include "PlatformPins.h"

#include <EventDispatcher.h>
#include <PeriodicCommandDispatcher.h>
#include <SerialLink.h>
#include <generated/AppBuildVersionInfo.h>

using namespace core;

int main()
{
    dispatcher::ISerialLink* serialLink(
        new dispatcher::SerialLink(UART0_TX, UART0_RX, UART_BAUDRATE));

    dispatcher::IEventDispatcher* eventQueue(new dispatcher::EventDispatcher());

    dispatcher::IPeriodicCommandDispatcher* dispatcher(
        new dispatcher::PeriodicCommandDispatcher(serialLink, eventQueue));

    ImplCommands commands(dispatcher);

    commands.setBoardName(BOARD_NAME);
    commands.setPlatformId(PLATFORM_ID);
    commands.setModePin(MODE_INT_PIN);
    commands.setBuildVersionInfo(g_application_version);

    /*!
     *  This is a blocking call and never returns until dispatcher->terminate() is called
     *  from a command.
     */
    dispatcher->run();

    delete dispatcher;
    delete eventQueue;
    delete serialLink;
}
