# setup correct 'target_cpu, mbed version and target_device'
check_environment(CHECK_RESULT)

if (NOT ${CHECK_RESULT})
    return()
endif()

# Project's name and version
# NOTE: Do not start the project name with a number.
# Change the project name and version in the following line.

project(abcdef VERSION 1.0.0)
# TODO change to https://cmake.org/cmake/help/v3.1/prop_tgt/TYPE.html
set(TARGET_TYPE binary)

set(SOURCES ${SOURCES}
    src/main.cpp
    src/ImplCommands.cpp
)

include(${PLATFORM_DIR_PATH}/embedded-strata-core/tools/CMakeModules/application_main.cmake)

target_include_directories(${PROJECT_NAME}
    PUBLIC include
    PRIVATE ${PROJECT_BINARY_DIR}
    PUBLIC ${PLATFORM_DIR_PATH}/embedded-strata-core/include
    PUBLIC ${PLATFORM_DIR_PATH}/embedded-strata-core/include/${TARGET_CPU}
    PUBLIC ${PLATFORM_DIR_PATH}/embedded-strata-core/ext_libs
)

# Debug stuff
# get_cmake_property(_variableNames VARIABLES)
# list (SORT _variableNames)
# foreach (_variableName ${_variableNames})
#    message(STATUS "${_variableName}=${${_variableName}}")
# endforeach()

# Add new component library names below for example "cat24C512"
target_link_libraries(${PROJECT_NAME} PRIVATE cat24c512 efm32gg command-dispatcher)

target_link_libraries(${PROJECT_NAME} PRIVATE mbed-os-${MBED_OS_VERSION} )
