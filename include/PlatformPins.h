/*
 *
 * Platform-specific definitions
 *
 */
#ifndef _PLATFORMPINS_H_
#define _PLATFORMPINS_H_

// Declare some useful functions
extern "C"{
void platform_enable_uart_irq();
void platform_disable_uart_irq();
void platformDetectConnectState();
}

#define BOARD_NAME "WaterHeater Template"
#define PLATFORM_ID "Template"
#define FIRMWARE_VERSION "0.1.20181204170300"

// Interrupt pin to detect if host is connected through USB
// If mode pin is not implemented on hardware, simply define it as NC.
#define MODE_INT_PIN PE15

// UART definitions; UART0 is used by platformInterface to communicate to Strata
#define UART0_TX        PB9     //  TX
#define UART0_RX        PB10    //  RX
#define UART_BAUDRATE   115200  //  This baud must not change. Strata is fixed

// link LED directly on waterheater, need function to enable this when Strata is connected
#define LINK_LED_PIN PF5

/*!
 * Component I2C addresses
 */
#define CAT24C512_I2C_ADDR 0x50

//EEPROM CAT24C512 size is 64kB
#define CAT24C512_CHIP_SIZE (64*1024)
#define CAT24C512_PAGE_SIZE 128

// I2C data nd clock pins
// Default I2C for EEPROM
#define I2C1_SDA_PIN     PE0
#define I2C1_SCL_PIN     PE1

#endif // _PLATFORMPINS_H_
